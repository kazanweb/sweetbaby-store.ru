//Доступный hamburger https://foxland.fi/simple-accessible-svg-menu-hamburger-animation
function hamburger(element, menu) {
	var button = document.getElementById(element),
		menu = document.getElementById(menu);
	button.onclick = function () {
		// Toggle class "opened". Set also aria-expanded to true or false.
		if (-1 !== button.className.indexOf('opened')) {
			button.className = button.className.replace(' opened', '');
			button.setAttribute('aria-expanded', 'false');
			menu.className = menu.className.replace(' active', '');
			menu.setAttribute('aria-expanded', 'false');
		} else {
			button.className += ' opened';
			button.setAttribute('aria-expanded', 'true');
			menu.className += ' active';
			menu.setAttribute('aria-expanded', 'true');
		}
	};
}

$(document).ready(function () {
	// проверяет подержку touch
	function is_touch_device() {  
		try {  
			document.createEvent("TouchEvent");
			return true;  
		} catch (e) {
			return false;  
		}
	}
	if (is_touch_device()) {
		$('.dragscroll').css('overflow','auto');
	}


	// nice scroll in page compare
	if (!is_touch_device()) {
		$(".js-compare-product-scroll").niceScroll({
			cursorcolor: '#8ab95f',
			cursorwidth: '12px',
			cursorborder: 'none',
			cursorborderradius: '6px',
			background: '#eef1f2',
			autohidemode: false,
			zindex: 999
		});
	}

	if (!is_touch_device()) {
		document.documentElement.classList.add('no-touch');
	}

	// header search
	$( "body" ).on("click", ".js-search-header-button", function(e) {
		var element = $( ".js-search-header-input" );
		if (element.hasClass("search-header__input-active") === false) {
			element.addClass("search-header__input-active");
			return false;
		}
	});

	// in news page search
	$( "#search-input" ).click(function(e) {
		var element = $( ".js-search__input" );
		if (element.width() === 0) {
			element.addClass("search__input_active");
			return false;
		}
	});

	// reset input in menu category form colors and price
	$( ".js-reset-input-button" ).click(function(e) {
		var wrapParentInputs = $(this).closest('.js-reset-input-wrap'); // родитель инпутов в которых будем обнулять
		wrapParentInputs.find('.js-reset-input:text').val('');
		wrapParentInputs.find('.js-reset-input:radio, .js-reset-input:checkbox')
			.prop('checked', false)
			.prop('selected', false);
		return false;
	});

	// popup-search set height
	// function setHeightPopupSearch() {
	// 	var element = $( "#js-popup-search-height" );
	// 	var element2 = $( "#js-popup-search-height-bottom" );
	// 	element.height('auto'); // сбрасываем высоту на авто для перерасчета высоты при ресайзе
	// 	var height1 = element.outerHeight();
	// 	var height2 = element2.outerHeight();
	// 	var result = height1 + height2;
	// 	element.height(result);
	// }
	// setHeightPopupSearch();
	// var resizeTimer;
	// $(window).on('resize', function(){
	// 	clearTimeout(resizeTimer);
	// 	resizeTimer = setTimeout(function() {
	  
	// 		setHeightPopupSearch();
				  
	// 	}, 200);
  	// });


	(function () {
		var element = $('.js-compare-sticky');
		var elementHeight = element.outerHeight();
		if (element.length) {
			var posititon = element.offset().top;
			$(window).scroll(function() {
				if($(window).scrollTop() > posititon) {
					$("#js-wrapper").addClass('compare-sticky');
				} else {
					$("#js-wrapper").removeClass('compare-sticky');
				}
			});
		}
	})();

	// compare-equal-height
	$(".js-compare-height-wrap .js-compare-height").each(function(i) {
		$('.js-compare-equal-height-'+i).matchHeight();
	});

	// exchange checkbox check functional in page exchange
	$('.js-exchange-of-goods-form-send').click(function(){
		var form = $(this).parents('form');
		var checkboxElementParent = form.find('.js-exchange-of-goods-form-checkbox-validate');
		var checkboxElement = form.find('.js-exchange-of-goodss-form-checkbox-input');

		if (checkboxElement.prop("checked")) {
			checkboxElementParent.removeClass('exchange-of-goods-form__checkbox_required');
		} else {
			checkboxElementParent.addClass('exchange-of-goods-form__checkbox_required');
			if (form[0].checkValidity()) { // если форма валидна по html5 то прерываем отправку
				return false;
			}
		}
	});

	// popup checkbox check functional
	$('body').on('click', '.js-popup-block__button', function(){
		var parentElement = $(this).parents('.js-popup-block');
		var checkboxElementParent = parentElement.find('.js-popup-block-checkbox');
		var checkboxElement = parentElement.find('.js-popup-block-checkbox-input');
		var form = parentElement.find('form');

		if (checkboxElement.prop("checked")) {
			checkboxElementParent.removeClass('popup-block__checkbox_required');
		} else {
			checkboxElementParent.addClass('popup-block__checkbox_required');
			if (form[0].checkValidity()) { // если форма валидна по html5 то прерываем отправку
				return false;
			}
		}
	});

	// content-acordion
	// функционал акардиона при загрузки делаем первый акардион активный
	$('.content-accordions__title').first().next().show();
	$('.content-accordions__item').first().addClass('content-accordions__item_active');
	// функционал акардиона при клике
	$('.content-accordions__title').click(function() {
		var element = $('.content-accordions__title');
		element.parent().removeClass('content-accordions__item_active');
		$(this).parent().addClass('content-accordions__item_active');
		if ($(this).next().is(":visible")) {
			var that = this;
			$(this).next().slideUp("slow", function() {
				$(that).parent().removeClass('content-accordions__item_active');
			});
		} else {
			element.next().slideUp();
			$(this).next().slideDown();
		}
	});

	// compare open close acordion visible in mobile
	(function(){
		var breakpoint = window.matchMedia('(max-width: 768px)');
		function breakpointChecker() {
			if (breakpoint.matches === true) {
				$('.js-characteristic-item').click(function() { // height 0 нужен чтобы корректно работал плагин syncscroll
					var that = $(this);
					if (that.next().outerHeight() !== 0) {
						that.removeClass('js-characteristic-open');
						that.next().animate({height: 0}, 400);
					} else {
						var el = that.next(),
						curHeight = el.outerHeight(),
						autoHeight = el.css('height', 'auto').height();
						el.height(curHeight).animate({height: autoHeight}, 400, function() {
							el.height('auto'); // после анимации убираем фиксированую высоту
						});
						$(this).addClass('js-characteristic-open');
					}
				});
			} else {
				$('.js-characteristic-item').off( "click" );
			}
		}
		breakpoint.addListener(breakpointChecker);
		breakpointChecker();
	})();
	

	// upload file button in page exchange
	// $('#js-exchange-of-goods-form-file').change(function() {
	// 	$('#js-exchange-of-goods-form__selected-filename').text(this.files[0].name);
	// });

	// quanity input
	function resize() {
		// устанавливаем ширину по умолчанию в 12px
		var int = 12;
		$(".js-quantity-input").each(function() {
			var reg = new RegExp('^[0-9]+$');
			if ($.trim(this.value) != '') {
				if (!reg.test(this.value)) {
					this.value = 1;
				}
			}
			var el = this;
			if (el.value.length > 1) {
				el.style.width = (el.value.length * int) + 'px';
			} else {
				el.style.width = int + 'px';
			}
		});
	}
	resize();

	// слушаем событие на элементе
	$('.js-quantity-input').on('input', function() { 
		resize();
	});

	// функия для обработки минус
	$('.js-button-minus').click(function () {
		var $input = $(this).parent().find('input');
		var count = parseInt($input.val()) - 1;
		count = count < 1 ? 1 : count;
		$input.val(count);
		$input.change();
		resize();
		return false;
	});
	
	// функия для обработки плюс
	$('.js-button-plus').click(function () {
		var $input = $(this).parent().find('input');
		$input.val(parseInt($input.val()) + 1);
		$input.change();
		resize();
		return false;
	});
	// quanity input end

	// tabs 
	new Tabs();

	// cart-product hidden
	$( ".js-cart__delete" ).click(function(e) {
		var element = $( this ).closest('.js-cart__product');
		element.find('.js-cart__product-disable').show();
		element.find('input').prop('disabled', true);
		return false;
	});

	// cart-product remove 
	$( ".js-cart__confirm-remove" ).click(function(e) {
		$( this ).closest('.js-cart__product').remove();
		return false;
	});

	// cart-product return 
	$( ".js-cart__return-cart" ).click(function(e) {
		var element = $( this ).closest('.js-cart__product');
		element.find('.js-cart__product-disable').hide();
		element.find('input').prop('disabled', false);
		return false;
	});

	// menu category
	$( ".js-menu-category__item" ).click(function(e) {
		var that = this;
		var visible = $( this ).find(".menu-category__sub-list").is(":visible");
		$( this ).addClass("menu-category__item_active");
		$( '.menu-category__item' ).removeClass("menu-category__item_close");
		if (visible) {
			$( this ).addClass("menu-category__item_close");
			setTimeout(function(){
				$( that ).removeClass('menu-category__item_active');
			}, 500);
		} else {
			setTimeout(function(){
				$( ".menu-category__item" ).not(that).removeClass('menu-category__item_active');
			}, 400);
			$( ".menu-category__item" ).not(that).addClass("menu-category__item_close");
			$( ".menu-category__sub-list" ).slideUp(500);
		}
		$( this ).find(".menu-category__sub-list").slideToggle(500);
		return false;
	});
	$(".menu-category__sub-list").click(function(e) {
		e.stopPropagation();
	});

	$(".js-menu-category__mobile-button").click(function() {
		$( ".menu-category" ).slideToggle();
	});

	// card slider
	if ($(".js-card__slider").length > 0) {
		$('.js-card__slider').each(function (index) {
			var parent = $(this).parents('.js-card-parent-slider');
			var mySwiper = new Swiper ($(this), {
				loop: false,
				slidesPerView: 'auto',
				slideClass: "swiper-slide",
				pagination: {
					el: parent.find('.js-card__slider-dots')[0],
				},
			});
		});
	}

	// sldier swipper with card border product
	if ($(".js-slider-card").length > 0) {
		var mySwiper = new Swiper ('.js-slider-card', {
			loop: false,
			slidesPerView: 'auto',
			spaceBetween: 16,
			slideClass: "slider-card__slider",
			scrollbar: {
				el: '.swiper-scrollbar',
				hide: false,	
				draggable : true,
				snapOnRelease: false
			},
			breakpoints: {
				900: {
					spaceBetween: 9
				},
				768: {
					spaceBetween: 12
				}
			},
		});
	}

	// sldier swipper article in news-detail page
	if ($(".article-block__slider").length > 0) {
		var mySwiper = new Swiper ('.article-block__slider', {
			loop: false,
			slidesPerView: 'auto',
			spaceBetween: 16,
			slideClass: "article-block__item",
		});
	}

	// sldier swipper in related-products page
	if ($(".related-products__slider").length > 0) {
		var mySwiper = new Swiper ('.related-products__slider', {
			loop: false,
			slidesPerView: 'auto',
			spaceBetween: 16,
			slideClass: "swiper-slide",
			scrollbar: {
				el: '.swiper-scrollbar',
				hide: false,	
				draggable : true,
				snapOnRelease: false
			},
		});
	}

	// sldier swipper in recently-viewed page
	if ($(".recently-viewed__slider").length > 0) {
		var mySwiper = new Swiper ('.recently-viewed__slider', {
			loop: false,
			slidesPerView: 'auto',
			spaceBetween: 16,
			slideClass: "swiper-slide",
			scrollbar: {
				el: '.swiper-scrollbar',
				hide: false,	
				draggable : true,
				snapOnRelease: false
			},
		});
	}

	if ($(".slider-banner-container").length > 0) {
		// sldier swipper header slider in main page
		var mySwiper = new Swiper ('.slider-banner-container', {
			slideClass: "slider__content",
			navigation: {
				nextEl: '.slider__arrow-right',
				prevEl: '.slider__arrow-left',
			},
			pagination: {
				el: '.slider__dots',
				clickable: true,
				renderBullet: function (index, className) {
					return '<li class="'+className+'"></li>';
				},
				bulletClass: "slider__dots-circle",
				bulletActiveClass: "slider__dots-circle_active"
			},
		});
	}

	if ($(".catalog__review-slider").length > 0) {
		// sldier review in catalog page
		var mySwiper = new Swiper ('.catalog__review-slider', {
			loop: false,
			slidesPerView: 2,
			spaceBetween: 16,
			slideClass: "swiper-slide",
			scrollbar: {
				el: '.swiper-scrollbar',
				hide: false,	
				draggable : true,
				snapOnRelease: false
			},
			breakpoints: {
				// when window width is <= 768px
				768: {
				  slidesPerView: 'auto',
				}
			  }
		});
	}
	

	// sldier swipper in product-detail page in head
	(function() {

		'use strict';

		if (!$(".product-detail__gallery-top").length > 0) {
			return;
		}
	  
		// breakpoint where swiper will be destroyed
		// and switches to a dual-column layout
		const breakpoint = window.matchMedia( '(min-width:1101px)' );
	  
		// keep track of swiper instances to destroy later
		var productDetailGalleryTop;
		var productDetailGalleryTop2;
		var productDetailGalleryThumbs;
		var productDetailGalleryThumbs2;
		var activeClass = 'js-product-detail__slider-is-selected';


		var breakpointChecker = function() {
			// else if a small viewport and single column layout needed
		  	if ( breakpoint.matches === true ) {
				// fire small viewport version of swiper
				if ( productDetailGalleryTop2 !== undefined ) {
					productDetailGalleryTop2.destroy( true, true );
				}
				if ( productDetailGalleryThumbs2 !== undefined ) {
					productDetailGalleryThumbs2.destroy( true, true );
				}
				
				return enableSwiper();

		  	// if larger viewport and multi-row layout needed
			} else if ( breakpoint.matches === false ) {
			  // clean up old instances and inline styles when available
				if ( productDetailGalleryTop !== undefined ) {
					productDetailGalleryTop.destroy( true, true );
				}
				if ( productDetailGalleryThumbs !== undefined ) {
					productDetailGalleryThumbs.destroy( true, true );
				}
				//initialization parameters
				productDetailGalleryTop2 = new Swiper('.product-detail__gallery-top', {
					slidesPerView: 1,
					spaceBetween: 0,
					slideClass: 'product-detail__img',
					navigation: {
						nextEl: '.js-product-detail__pagination-relative .swiper-button-next',
						prevEl: '.js-product-detail__pagination-relative .swiper-button-prev',
					},
					// on: {
					// 	slideChange: function () {
					// 		var activeIndex = productDetailGalleryTop2.activeIndex;
					// 		$(productDetailGalleryThumbs2.slides).removeClass(activeClass);
					// 		$(productDetailGalleryThumbs2.slides).eq(activeIndex).addClass(activeClass);
					// 		productDetailGalleryThumbs2.updateSlides();
					// 		productDetailGalleryThumbs2.slideTo(activeIndex,500, false);
					// 	},
					// },
				});
				productDetailGalleryThumbs2 = new Swiper('.product-detail__gallery-thumbs', {
					spaceBetween: 10,
					centeredSlides: false,
					slidesPerView: 'auto',
					slideClass: 'product-detail__small-img',
					touchRatio: 0.2,
					slideToClickedSlide: true,
					centeredSlides: true,
					breakpoints: {
						// when window width is <= 400px
						400: {
							spaceBetween: 5
						}
					}
					// on: {
					// 	tap: function () {
					// 		var clicked = productDetailGalleryThumbs2.clickedIndex;
					// 		$(productDetailGalleryThumbs2.slides).removeClass(activeClass);
					// 		$(productDetailGalleryThumbs2.clickedSlide).addClass(activeClass);
					// 		productDetailGalleryThumbs2.updateSlides();
					// 		productDetailGalleryTop2.slideTo(clicked,500, false);
					// 	},
					// },
				});

				productDetailGalleryTop2.controller.control = productDetailGalleryThumbs2;
				productDetailGalleryThumbs2.controller.control = productDetailGalleryTop2;

				// or/and do nothing
				return;
	  
			}
	  
		};
	  
		var enableSwiper = function() {
			productDetailGalleryTop = new Swiper('.product-detail__gallery-top', {
				slidesPerView: 1,
				spaceBetween: 0,
				slideClass: 'product-detail__img',
				direction:'vertical',
				// on: {
				// 	slideChange: function () {
				// 		var activeIndex = productDetailGalleryTop.activeIndex;
				// 		$(productDetailGalleryThumbs.slides).removeClass(activeClass);
				// 		$(productDetailGalleryThumbs.slides).eq(activeIndex).addClass(activeClass);
				// 		productDetailGalleryThumbs.updateSlides();
				// 		productDetailGalleryThumbs.slideTo(activeIndex,500, false);
				// 	},
				// },
			});
			productDetailGalleryThumbs = new Swiper('.product-detail__gallery-thumbs', {
				spaceBetween: 10,
				centeredSlides: false,
				slidesPerView: 'auto',
				slideClass: 'product-detail__small-img',
				touchRatio: 0.2,
				direction: 'vertical',
				centeredSlides: true,
				slideToClickedSlide: true,
				// on: {
				// 	tap: function () {
				// 	  	var clicked = productDetailGalleryThumbs.clickedIndex;
				// 		$(productDetailGalleryThumbs.slides).removeClass(activeClass);
				// 		$(productDetailGalleryThumbs.clickedSlide).addClass(activeClass);
				// 		productDetailGalleryThumbs.updateSlides();
				// 		productDetailGalleryTop.slideTo(clicked,500, false);
				// 	},
				// },
			});

			productDetailGalleryTop.controller.control = productDetailGalleryThumbs;
			productDetailGalleryThumbs.controller.control = productDetailGalleryTop;
		};
	  
		// keep an eye on viewport size changes
		breakpoint.addListener(breakpointChecker);
	  
		// kickstart
		breakpointChecker();
	  
	})();

	// sldier tags in catalog page min-width 800px
	(function() {

		'use strict';

		if (!$(".js-catalog-tags").length > 0) {
			return;
		}
	  
		// breakpoint where swiper will be destroyed
		// and switches to a dual-column layout
		const breakpoint = window.matchMedia( '(min-width:801px)' );
	  
		// keep track of swiper instances to destroy later
		var mySwiper;
		var myScroll;
		var wrapper = $('.js-catalog-tags-wrap');
		// var mySwiper2;

		var breakpointChecker = function() {
			// else if a small viewport and single column layout needed
		  	if ( breakpoint.matches === true ) {
				if ( myScroll !== undefined ) {
					myScroll.destroy();
					myScroll = null;
				}
				wrapper.width('auto'); // убираем фиксированую ширину которую устанавливаем при разрешении меньше 800px
				return enableSwiper();

		  	// if larger viewport and multi-row layout needed
			} else if ( breakpoint.matches === false ) {
			  	// clean up old instances and inline styles when available
				if ( mySwiper !== undefined ) {
					mySwiper.destroy( true, true );
				}

				// устанавливаем ширину элементу чтобы была прокрутка и элемемнты в два ряда
				var element = $('.js-catalog-tag');
				var countElements = $( element ).length; // количество элементов
				var fullWidthElements = 0;
				var slice = countElements / 2;
				function setWidthWrap() {
					var sliceFirstWidth = 0;
					var sliceSecondWidth = 0;
					slice = Math.ceil(slice);
					for(var i = 0; i < slice; i++) {
						sliceFirstWidth += element.eq(i).outerWidth(true);
					}
					for(var i = slice; i < countElements; i++) {
						sliceSecondWidth += element.eq(i).outerWidth(true);
					}
					if (sliceFirstWidth > sliceSecondWidth) {
						sliceFirstWidth = Math.ceil(sliceFirstWidth);
						wrapper.width(sliceFirstWidth);
					} else {
						sliceSecondWidth = Math.ceil(sliceSecondWidth);
						wrapper.width(sliceSecondWidth);
					}
				}
				element.each(function(i){
					fullWidthElements += element.eq(i).outerWidth(true);
				});
				fullWidthElements = Math.ceil(fullWidthElements / 2);

				function setOrNotWidthWrap() {
					wrapper.width('auto');
					var wrapperWidth = Math.ceil(wrapper.width());
					if (fullWidthElements > wrapperWidth) {
						setWidthWrap();
					} else {
						var firstWidthRow = 0;
						var posititon = 0; // позиция если элемент не помешается в первый ряд 
						element.each(function(i) {
							if (firstWidthRow > wrapperWidth) { // нам нужна позиция элента который переходит на новую строку
								posititon = i;
								firstWidthRow = 0;
							} else {
								if (posititon > 0) {
									var secondWidthRow = 0;
									for (var i = posititon; i <= element.length; i++) {
										secondWidthRow += element.eq(i - 1).outerWidth(true);
									}
									if (secondWidthRow > wrapperWidth) {
										setWidthWrap();
									}
								} else {
									firstWidthRow += element.eq(i).outerWidth(true);
								}
							}
						});
						element.css('margin-right', 11);
					}
				}
				setOrNotWidthWrap();

				var doit;
				$(window).resize(function() {
					setOrNotWidthWrap();
					clearTimeout(doit);
					doit = setTimeout(setOrNotWidthWrap, 200);
				});

				// plugin для скрола контента
				myScroll = new IScroll('.js-catalog-tags', {
					disablePointer: true,
					disableTouch: false,
					disableMouse: false,
					mouseWheel: false,
					scrollX: true,
					scrollY: false,
					preventDefault: true,
					click: true,
				});
				return;
			}
	  
		};
	  
		var enableSwiper = function() {
			// sldier tags in catalog page
			mySwiper = new Swiper ('.js-catalog-tags', {
				loop: false,
				slidesPerView: 'auto',
				spaceBetween: 11,
				slideClass: "js-catalog-tag",
				navigation: {
					nextEl: '.js-catalog-tags-next',
					prevEl: '.js-catalog-tags-prev',
				}
			});
		};
	  
		// keep an eye on viewport size changes
		breakpoint.addListener(breakpointChecker);
	  
		// kickstart
		breakpointChecker();
	  
	})(); /* IIFE end */


	// slider in main page block actual-news
	(function() {

		'use strict';

		if (!$(".actual-news-swiper").length > 0) {
			return;
		}
	  
		// breakpoint where swiper will be destroyed
		var breakpoint = window.matchMedia( '(min-width: 581px) and (max-width:768px)' );
		var breakpoint2 = window.matchMedia( '(max-width: 580px)' );
	  
		// keep track of swiper instances to destroy later
		var mySwiper;
		var mySwiper2;
	  
		var breakpointChecker = function() {
	  
			// else if a small viewport and single column layout needed
		  	if ( breakpoint.matches === true ) {
				// fire small viewport version of swiper
				return enableSwiper();

		  	// if larger viewport
			} else if ( breakpoint.matches === false ) {

			 	// clean up old instances and inline styles when available
				if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
				return;
			}
	  
		};

		var breakpointChecker2 = function() {
	  
			// else if a small viewport and single column layout needed
		  	if ( breakpoint2.matches === true ) {
				// fire small viewport version of swiper
				return enableSwiper2();

		  	// if larger viewport
			} else if ( breakpoint2.matches === false ) {
			  // clean up old instances and inline styles when available
				if ( mySwiper2 !== undefined ) mySwiper2.destroy( true, true );

				return;
			}
	  
		};

		var options = {
			loop: false,
			slideClass: "mobile-slider-item",
			spaceBetween: 12,
			slidesPerView: 'auto',
			navigation: {
				nextEl: '.actual-news__slider-buttons .border-slider-button__right',
				prevEl: '.actual-news__slider-buttons .border-slider-button__left',
				disabledClass: 'border-slider-button__hidden',
			}
		}
	  
		var enableSwiper = function() {
			mySwiper = new Swiper ('.actual-news-swiper', options);
		};

		var enableSwiper2 = function() {
			var element = $('.actual-news .mobile-slider-item');
			var elementWrap = $('#js-actual-news-height');
			var lastColumnIndex;
			var copyOptions = Object.assign({}, options);
			copyOptions.slidesPerColumnFill = 'row';
			copyOptions.slidesPerColumn = 2;
			copyOptions.slidesPerView = 1;
			copyOptions.spaceBetween = 14;
			copyOptions.navigation = {
				nextEl: '.actual-news__slider-buttons2 .border-slider-button__right',
				prevEl: '.actual-news__slider-buttons2 .border-slider-button__left',
				disabledClass: 'border-slider-button__hidden',
			}
			// copyOptions.on = { // нужен для autoHeight slidera
			// 	init: function () {
			// 		var slideCount = element.length;
			// 		if (slideCount > 2) { // если количество слайдов больше двух
			// 			if (slideCount % 2 !== 0) { // если количество слайдов не четное получаем последний слайд чтобы установить на враппер фиксированйю высоту
			// 				var lastColumn = Math.floor((slideCount / 2));
			// 				element.each(function(i) {
			// 					if ($(this).data('swiperColumn') == lastColumn) {
			// 						lastColumnIndex = $(this).index();
			// 					}
			// 				});
			// 			}
			// 		}
			// 	},
			// 	slideChange: function() { // если последний элемент и не имеет второго нижнего блока то изменяем высоту на фиксрованую
			// 		var activeIndex = mySwiper2.activeIndex;
			// 		if (activeIndex == lastColumnIndex) {
			// 			var height = element.eq(activeIndex).height();
			// 			elementWrap.height(height);
			// 		} else {
			// 			elementWrap.height('auto');
			// 		}
			// 	}
			// }
			mySwiper2 = new Swiper ('.actual-news-swiper', copyOptions);
		};
	  
	  
		// keep an eye on viewport size changes
		breakpoint.addListener(breakpointChecker);
		breakpoint2.addListener(breakpointChecker2);
	  
		// kickstart
		breakpointChecker();
		breakpointChecker2();
	  
	})(); /* IIFE end */


	(function() {

		'use strict';

		if (!$(".popular-review-swiper").length > 0) {
			return;
		}

		if (!$(".popular-products-swiper").length > 0) {
			return;
		}
	  
		// breakpoint where swiper will be destroyed
		var breakpoint = window.matchMedia( '(max-width:768px)' );
	  
		// keep track of swiper instances to destroy later
		var mySwiper;
		var mySwiper2;
	  
		var breakpointChecker = function() {
	  
			// else if a small viewport and single column layout needed
		  	if ( breakpoint.matches === true ) {
				// fire small viewport version of swiper
				return enableSwiper();

		  	// if larger viewport
			} else if ( breakpoint.matches === false ) {

			  // clean up old instances and inline styles when available
				if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
				if ( mySwiper2 !== undefined ) mySwiper2.destroy( true, true );

				return;
			}
	  
		};

		var options = {
			loop: false,
			slideClass: "mobile-slider-item",
			spaceBetween: 12,
			slidesPerView: 'auto',
			navigation: {
				nextEl: '.border-slider-button__right',
				prevEl: '.border-slider-button__left',
				disabledClass: 'border-slider-button__hidden',
			}
		}
	  
		var enableSwiper = function() {
			// Переопределяем кнопки чтобы не конфликтавали
			options.navigation.nextEl = '.popular-review__slider-buttons .border-slider-button__right';
			options.navigation.prevEl = '.popular-review__slider-buttons .border-slider-button__left';
			mySwiper = new Swiper ('.popular-review-swiper', options);
			
			// Переопределяем кнопки чтобы не конфликтавали
			options.navigation.nextEl = '.popular-products__slider-buttons .border-slider-button__right';
			options.navigation.prevEl = '.popular-products__slider-buttons .border-slider-button__left';
			mySwiper2 = new Swiper ('.popular-products-swiper', options);
		};
	  
		// keep an eye on viewport size changes
		breakpoint.addListener(breakpointChecker);
	  
		// kickstart
		breakpointChecker();
	  
	})(); /* IIFE end */


	// match-height
	$('.equal-height').matchHeight();

	// Ratings
	new Ratings({
		element: document.querySelector('.js-ratings__section'),	// передаем элемент
		countRate: 5,							//  кол-во оценок, необязательный параметр, по умолчанию стоит 5
		clickFn: function(index) {
			alert(index);
		}
	});


	// popup add coments
	var PopupAddCommentsHtml = $('#js-popup-add-comments-content');
	var popupAddComments = new Popup();

	$('#js-popup-add-comments').click(function () {
		popupAddComments.options({
			closeShow: false,
			closeButtons: '.js-popup-close',
		});
		popupAddComments.html(PopupAddCommentsHtml);
		popupAddComments.show();
	});

	// popup callback
	var PopupCallbackHtml = $('#js-popup-callback-content');
	var popupCallback = new Popup();

	$('#js-popup-callback').click(function () {
		popupCallback.options({ 
			closeShow: false,
			closeButtons: '.js-popup-close',
		});
		popupCallback.html(PopupCallbackHtml);
		popupCallback.show();
		return false;
	});

	// popup complaint
	var PopupComplaintHtml = $('#js-popup-complaint-content');
	var popupComplaint = new Popup();

	$('#js-popup-complaint').click(function () {
		popupComplaint.options({
			closeShow: false,
			closeButtons: '.js-popup-close',
		});
		popupComplaint.html(PopupComplaintHtml);
		popupComplaint.show();
		return false;
	});

	// $(".js-combox").combox({
	// 	startFn: function(li, index, combox) {

	// 		this.input = combox.getElementsByTagName("input")[0];

	// 		this.input.value = li.getAttribute("value");

	// 	},
	// 	changeFn: function(li, index, combox) {

	// 		var _this = this;

	// 		this.input.value = li.getAttribute("value");

	// 	}
	// });

	hamburger('js-hamburger', "js-menu");

	$("[type=tel]").mask("999 999-99-99");
	$(".js-order-number").mask("999-999-99-99");

	// Прибивка адаптивного футера к низу
	(function (footerSelector, wrapperSelector) {

		var footer = document.querySelector(footerSelector);
		var wrapper = document.querySelector(wrapperSelector);
		var height;
		var setSize;

		if (!wrapper || !footer) {
			return false;
		}

		setSize = function () {

			height = footer.offsetHeight;

			wrapper.style.paddingBottom = height + 'px';
			footer.style.marginTop = (height * (-1)) + 'px';

		}

		setSize();

		window.addEventListener('resize', setSize, false);

	})('#js-footer', '#js-wrapper');

});