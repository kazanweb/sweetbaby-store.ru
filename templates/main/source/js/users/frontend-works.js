$(document).ready(function () {

	$(document).on('click', '[data-ajax]', function (e) {
		e.stopPropagation();
		e.preventDefault();
		$.get(this.getAttribute('data-url'), function (response) {
			globalPopups.options({ // если не добавить опции при клике отображается крестик дефолтный
				closeShow: false,
				closeButtons: '.js-popup-close',
			});
			globalPopup.html(response).show();
		});
	});

	// popup add coments reply
	$(document).on('click', '.js-popup-add-comments-reply', function () {
		var replyId = $(this).data('commentId'); // id который нужно подставить в popup
		$.get(this.getAttribute('data-url'), function (response) {
			globalPopups.options({ // если не добавить опции при клике отображается крестик дефолтный
				closeShow: false,
				closeButtons: '.js-popup-close',
			});
			globalPopups.html(response).show();
		});
	});

	// оформить заказ проверка метода оплаты
	$( ".js-send-order" ).click(function(e) {
		var flag = false;
		var element = $( '.js-method-of-payment-validate' );
		element.each(function() { // проверяем выбран или нет способ оплаты
			if ($(this).prop("checked") === true) {
				flag = true;
				return;
			}
		});
		if (flag && $('#js-order-form')[0].checkValidity()) { // если проверка прошла успешно
			$('.js-method-of-payment-validate-message').hide();
			$.get(this.getAttribute('data-url'), function (response) {
				globalPopups.options({ // если не добавить опции при клике отображается крестик дефолтный
					closeShow: false,
					closeButtons: '.js-popup-close',
				});
				globalPopups.html(response).show();
			});
			e.preventDefault();
		} else {
			if (flag) {
				$('.js-method-of-payment-validate-message').hide();
			} else {
				$('.js-method-of-payment-validate-message').show();
			}
			if ($('#js-order-form')[0].checkValidity()) { // нужна чтобы не остановить проверку дефолтную html5
				e.preventDefault();
			}
		}
	});

});